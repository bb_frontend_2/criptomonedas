import React,{Fragment, useState} from 'react';
import styled from '@emotion/styled';

const  Label = styled.label`
    font-family: 'bebas Neue', cursive;
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 2.4rem;
    margin-top: 2rem;
    display: block;
`
const Select = styled.select`
    width: 100%;
    display: block;
    padding: 1rem;
    --webkit-appearance: none;
    border-radius: 10px;
    border:none;
    font-size:1rem;
`
const useMoneda = (label,stateInicial, MONEDAS)=>{
    const [state, actualizarState]= useState(stateInicial);
    const Seleccionar = () => {
        return(
            <Fragment>
            <Label>{label}</Label>
            <Select
                onChange={(e)=>actualizarState(e.target.value)}
                value={state}
            >
                <option>--seleccione una opcion--</option>
                {
                    MONEDAS.map((moneda)=>(
                        <option key={moneda.codigo} value={moneda.codigo}>{moneda.nombre}</option>
                    ))
                }
            </Select>
        </Fragment>

        )
}
return([state, Seleccionar, actualizarState ]);
    //retornar state
}
export default useMoneda;
