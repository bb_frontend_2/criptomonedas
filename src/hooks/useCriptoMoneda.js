import React,{Fragment, useState} from 'react';
import styled from '@emotion/styled';

const  Label = styled.label`
    font-family: 'bebas Neue', cursive;
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 2.4rem;
    margin-top: 2rem;
    display: block;
`
const Select = styled.select`
    width: 100%;
    display: block;
    padding: 1rem;
    --webkit-appearance: none;
    border-radius: 10px;
    border:none;
    font-size:1rem;
`
const useSelectCripto = (label, inicial, opciones)=>{
    console.log(opciones);
    const [state, actualizarState]= useState(inicial);

    const SelectCripto = () => {
        return(
            <Fragment>
            <Label>{label}</Label>
            <Select
                onChange={(e)=>actualizarState(e.target.value)}
                value={state}
            >
                <option>--seleccione una opcion--</option>
                {opciones.map((opc)=>(
                    <option key={opc.CoinInfo.Id}  value={opc.CoinInfo.Name}>{opc.CoinInfo.FullName}</option>
                ))}
            </Select>
        </Fragment>

        )
}
return([state, SelectCripto, actualizarState ]);
    //retornar state
}
export default useSelectCripto;
