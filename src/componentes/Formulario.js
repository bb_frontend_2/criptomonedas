import React,{useEffect, useState} from 'react';
import styled from '@emotion/styled';
import useMoneda from '../hooks/useMonenda';
import useCriptoMoneda from '../hooks/useCriptoMoneda'
import userEvent from '@testing-library/user-event';
import Error from './Error'
import axios from 'axios';
const Boton = styled.input`
    margin-top:20px;
    font-weight: bold;
    font-size:  20px;
    padding: 10px;
    background-color: #66a2fe;
    border: none;
    width:100%;
    border-radius: 10px;
    color: #fff;
    transition: background-color 0.3s ease;
    &:hover{
        background-color: #326ac0;
        cursor: pointer;
    }

`;

function Formulario({guardarMoneda, guardarCriptomoneda}) {
    // state del listado de criptomonedas
    const [error, guardarError]= useState(false);
    const [listadoCripto, guardarCriptomonedas]= useState([]);
    const MONEDAS = [
        {codigo: 'USD', nombre: 'dolar de Estados Unidos'},
        {codigo: 'MXN', nombre: 'peso Mexicano'},
        {codigo: 'GBP', nombre: 'Libra Exterlina'},
    ]
    const [moneda, SelecMonedas] = useMoneda('Elige Tu Moneda','', MONEDAS);
    const [criptoMoneda,  SelectCripto] = useCriptoMoneda('Elige tu criptomoneda ','', listadoCripto);
    // ejecutar llamado a la api
    const cotizarMoneda = (e)=>{
        e.preventDefault();
    //  validar si los campos estan llenos
        if(moneda === ''|| criptoMoneda === ''){
            guardarError(true);
            return;
        }
            guardarError(false);

            // pasar los datos al state principal

            guardarMoneda(moneda)
            guardarCriptomoneda(criptoMoneda);
    }
    // ejecutar el llamado a la api
    useEffect(()=>{
        const consultarApi = async()=>{
            const url= "https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD";
            const resultado = await axios.get(url);
            guardarCriptomonedas(resultado.data.Data);
        }
        consultarApi();
    }, [])
    return (
        <form
            onSubmit={cotizarMoneda}
        >
            {(error)?(<Error mensaje= "Todos Los Campos Son Obligatorios"/>): (null)}
            <SelecMonedas/>
            <SelectCripto/>
            <Boton
                type="submit"
                value="Calcular"
            ></Boton>
        </form>
    );
}

export default Formulario;