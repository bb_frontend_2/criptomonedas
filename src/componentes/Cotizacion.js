import React from 'react';
import styled from '@emotion/styled';

const ResultadoDiv = styled.div`
    color: #fff;
    font-family: Arial, Helvetica, sans-serif;
`;
const Parrafo = styled.p`
    height: 51px;
    line-height: 51px;
    background-color: gray;
    border-radius: 0.4rem;
    padding-left: 13px;

    span{
        font-weight: bold;

    }
`;
const Precio = styled.p`
    font-size: 30px;
    height: 60px;
    line-height: 60px;
    text-align: center;
    border-radius: 5px;
    background-color: darkcyan;

`;

function Cotizacion({resultado}) {
     if(Object.keys(resultado).length === 0)  return null;

    console.log(resultado);
    return (
        <ResultadoDiv>
            <Precio>El precio es de:<span>{resultado.PRICE}</span></Precio>
            <Parrafo>El precio mas alto del dia:<span>{resultado.HIGHDAY}</span></Parrafo>
            <Parrafo>El precio mas bajo del dia :<span>{resultado.LOWDAY}</span></Parrafo>
            <Parrafo>Variacion en las ultimas 24 horas:<span>{resultado.CHANGEPCT24HOUR}</span></Parrafo>
            <Parrafo>Ultima actualizacion :<span>{resultado.LASTUPDATE}</span></Parrafo>
        </ResultadoDiv>
    );
}

export default Cotizacion;