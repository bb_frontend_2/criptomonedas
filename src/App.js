import React, {useState, useEffect} from 'react';
import styled from '@emotion/styled';
import imagen from './cryptomonedas.png';
import Formulario from './componentes/Formulario';
import axios from 'axios';
import Cotizacion from './componentes/Cotizacion';
import Spinner from './componentes/Spinner';

const  Contenedor = styled.div`
  max-width: 900px;
  margin: 0 auto;
  @media(min-width: 992px){
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    column-gap: 2rem;
  }
`
const Imagen= styled.img`
  max-width: 100%;
  margin-top: 5rem;
`
 const  Heading = styled.h1`
  font-family: 'Bebas neue', cursive;
  color: #fff;
  text-align: left;
  font-weight: 700;
  font-size: 50px;
  margin-bottom: 50px;
  margin-top: 80px;

  &::after{
    content: '';
    width:100px;
    height:6px;
    background-color:  #66a2fe;
    display: block;
  }
 `
function App() {
  const [moneda,  guardarMoneda] = useState('');
  const [criptoMoneda, guardarCriptomoneda]= useState('');
  const [resultado, guaradarResultado]= useState({});
  const [cargando, guardarCargando]= useState(false);
  useEffect(()=>{
    // evita la ejecucion la primera vez
    if(moneda===''){
      return;
    }
    // consultar la api para  optener la  cotizacion
    const cotizarCriptomoneda =async()=>{

      const url=`https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptoMoneda}&tsyms=${moneda}`;
      const consulta = await axios.get(url);
      guardarCargando(true);
      //mostrar spinner
      setTimeout(() => {
        guardarCargando(false);
        guaradarResultado(consulta.data.DISPLAY[criptoMoneda] [moneda] );
      }, 3000);
    }
    cotizarCriptomoneda();
  },[moneda, criptoMoneda]);
  //mostrar spinner o resultado
  const componente = (cargando)?(<Spinner/>):(<Cotizacion
    resultado = {resultado}
  />)
  return (
    <div className="App">
      <Contenedor>
        <div>
          <Imagen
            src={imagen}
            alt ="imagen de cripto monedas"
            />
        </div>
        <div>
          <Heading>Cotiza Criptomonedas Al Instante</Heading>
          <Formulario
            guardarMoneda = {guardarMoneda}
            guardarCriptomoneda = {guardarCriptomoneda}
          />
          {componente}

        </div>
      </Contenedor>
    </div>
  );
}
export default App;
